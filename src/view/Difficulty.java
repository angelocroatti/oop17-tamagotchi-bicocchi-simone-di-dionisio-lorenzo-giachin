package view;

/**
 *
 */
public enum Difficulty {
    /**
     * 
     */
    EASY,

    /**
     * 
     */
    MEDIUM,

    /**
     * 
     */
    EXTREME;
}
