//CHECKSTYLE:OFF
package test.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

import controller.Controller;
import controller.TamagotchiController;
import model.Model;
import model.ModelImpl;
import model.character.Stats;
import model.container.Box;
import model.container.Item;
import model.ranking.AbstractCharacter;

public class ModelTest {

    Stats happiness = new Stats("Happiness", 1000);
    Stats hungry = new Stats("Hungry", 1000);
    Stats health = new Stats("Health", 1000);
    Stats cleanness = new Stats("Cleanness", 1000);
    Item ball = new Item("Ball", 20, 5, "ball.png");
    Box ballBox = new Box(ball);
    Item strawberry = new Item("Strawberry", 30, 3,"strawberry.png");
    Item chocolate = new Item("Chocolate", 100, 8,"chocolate.png");
    Item hamburger = new Item("Hamburger", 50, 5, "hamburger.png");
    Box strawBox = new Box(strawberry);
    Box chocoBox = new Box(chocolate);
    Box hambuBox = new Box(hamburger);
    List<Stats> list = new LinkedList<>(Arrays.asList(happiness, hungry, health, cleanness));

    @Test
    public void testCharacter() {
        Model model = new ModelImpl();
        Controller controller = new TamagotchiController(model);
        controller.loadStartInformation();
        list.forEach(e -> assertTrue(controller.getStats().contains(e)));
        controller.getInventory().values().forEach(e -> assertTrue(e.isEmpty()));
        controller.buy(ball.getName());
        assertTrue(controller.getInventory().get(happiness.getName()).contains(ballBox));
        assertTrue(!controller.getInventory().isEmpty());
        assertEquals(controller.getBalance(), 95);
        controller.setMainItem(happiness.getName(), ball.getName());
        assertTrue(controller.getMainItem(happiness.getName()).equals("ball.png"));
        controller.buy(hamburger.getName());
        controller.checkAndSetMainItem();
        assertTrue(controller.getMainItem(hungry.getName()).equals("hamburger.png"));
    }

    @Test
    public void testShop() {
        Model model = new ModelImpl();
        model.initializeContainers(new HashMap<String, List<Box>>());
        Controller controller = new TamagotchiController(model);
        // System.out.println(controller.getShop());
        assertEquals(controller.getShop(), new HashMap<String, List<Box>>());
        controller.loadStartInformation();
        controller.buy(hamburger.getName());
        controller.checkAndSetMainItem();
        controller.buy(chocolate.getName());
        controller.checkAndSetMainItem();
        controller.buy(strawberry.getName());
        controller.checkAndSetMainItem();
        assertTrue(controller.getMainItem("Hungry").equals(hambuBox.getFirst().getUrl()));
        controller.modStat(hungry.getName());
        controller.checkInventory();
        assertTrue(controller.getMainItem("Hungry").equals(chocoBox.getFirst().getUrl()));
        assertEquals(controller.getShop().size(), 4);
        list.forEach(e -> assertEquals(controller.getShop().get(e.getName()).size(), 5));
    }

    @Test
    public void testRanking() {
        Model model = new ModelImpl();
        Controller controller = new TamagotchiController(model);
        System.out.println(controller.getRanking());
        assertTrue(controller.getRanking().isEmpty());
        controller.loadStartInformation();
        assertTrue(!controller.modAllStats(-1000));
        controller.addRanking();
        assertTrue(controller.getRanking().contains(new AbstractCharacter(null, 0)));
        assertEquals(controller.getRanking().size(), 1);

    }

}
